package samuelpalmer.floatingrotation;

public abstract class Ids {

    public static abstract class Notification {

        public static final int SERVICE = 1;
        public static final int PERMISSION_NEEDED = 2;

    }

    public static abstract class PendingIntent {

        public static final int TOGGLE_BUTTON_VISIBILITY = 2;
        public static final int TOGGLE_ROTATION = 3;
        public static final int REQUEST_PERMISSIONS = 4;

    }

}
